﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using TokenAuthenticationWebApi.Models;

[assembly: OwinStartup(typeof(TokenAuthenticationWebApi.Startup))]
namespace TokenAuthenticationWebApi
{
    // Dans cette classe, nous allons configurer le serveur d'autorisations OAuth.
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Activer le partage de ressources entre origines (CORS) pour les requêtes utilisant un navigateur de différents domaines
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            var options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                // Le chemin pour générer le Token
                TokenEndpointPath = new PathString("/token"),
                // Définition du délai d'expiration du jeton (24 heures)
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                // la classe MyAuthorizationServerProvider validera les informations d'identification de l'utilisateur
                Provider = new MyAuthorizationServerProvider()
            };
            //Générations des Tokens 
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }
    }
}
