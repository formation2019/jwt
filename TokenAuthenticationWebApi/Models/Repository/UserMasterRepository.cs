﻿using System;
using System.Linq;
using TokenAuthenticationWebApi.Models.Contexts;

namespace TokenAuthenticationWebApi.Models.Repository
{
    public class UserMasterRepository : IDisposable
    {
        SECURITY_DBEntities context = new SECURITY_DBEntities();

        // Cette méthode est utilisée pour vérifier et valider les informations d'identification de l'utilisateur.
        public UserMaster ValidateUser(string username, string password)
        {
            return context.UserMasters
                .FirstOrDefault(u => u.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                    && u.UserPassword == password);
        }

        public void Dispose()
        {
            context.Dispose();

        }
    }
}